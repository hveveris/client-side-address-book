var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('compile', function(){

    browserify('./app/address-book.app.js')    
    .bundle()        
    .pipe(source('address-book.app.min.js'))        
    .pipe(gulp.dest('./app/'));

});

gulp.task('run', ['compile'], function() {

  gulp.watch(['*.html', './assets/**/*.*', './app/**/*.js', './app/**/*.html'], {cwd: './'}, reload);

  browserSync({
    server: {
      baseDir: './'
    }
  });

});