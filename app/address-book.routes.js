(function() {
  'use strict';

  angular
    .module('addressBook')
    .config(function($routeProvider) {

      $routeProvider
        .when('/', {
          controller: 'ContactListController as contactList',
          templateUrl: 'app/components/contact-list/contactListView.html'            
        })
        .when('/edit/:id', {
          controller: 'EditContactController as contactController',
          templateUrl: 'app/components/contact-add/contactFormView.html'
        })
        .when('/add', {
          controller: 'AddContactController as contactController',
          templateUrl: 'app/components/contact-add/contactFormView.html'
        })
        .otherwise({
          redirectTo: '/'
        });
    })
})();