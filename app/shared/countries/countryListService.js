(function() {

	angular
		.module('addressBook')
		.service('countriesListService', function() {

			this.list = require("country-list")().getNames();
		});
})();