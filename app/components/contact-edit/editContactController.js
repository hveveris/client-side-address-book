(function() {
	'use strict';

	angular
		.module('addressBook')
		.controller('EditContactController', ['$location',
			'$routeParams',
			'countriesListService',
			'addressBookStorageService',
			EditContactController
		]);

	function EditContactController($location, $routeParams, countriesListService, addressBookStorageService) {

		var findIndex = require('lodash/array/findIndex');

		var contactId = $routeParams.id;
		var contacts = addressBookStorageService.getContacts();
		var index = findIndex(contacts, function(o) {
			return o.id == contactId;
		});

		this.mode = "edit";
		this.saveButtonTitle = "Save";

		this.contact = contacts[index];
		this.countryList = countriesListService.list;

		this.save = function() {
			addressBookStorageService.updateContact(this.contact);
			$location.path('/');
		}

		this.delete = function() {
			addressBookStorageService.deleteContact(contactId);
			$location.path('/');
		}
	}

})();