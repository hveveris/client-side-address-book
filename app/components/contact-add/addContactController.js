(function() {
	'use strict';

	angular
		.module('addressBook')
		.controller('AddContactController', ['$location', 'countriesListService', 'addressBookStorageService', AddContactController]);

	function AddContactController($location, countriesListService, addressBookStorageService) {

		var AddressBookContact = require("../../../assets/js/contacts/addressbookcontact.js");

		this.mode = "add";
		this.saveButtonTitle = "Add";

		this.countryList = countriesListService.list;
		this.contact = new AddressBookContact();
		this.contact.country = this.countryList[0];


		this.save = function() {

			addressBookStorageService
				.addContact(this.contact);

			$location.path('/');
		}
	}

})();