(function() {
  'use strict';

  angular
    .module('addressBook')
    .config(function(localStorageServiceProvider) {
      localStorageServiceProvider
        .setPrefix('addressBook');
    })
    .service('addressBookStorageService', ['localStorageService', AddressBookStorageService]);

  function AddressBookStorageService(localStorageService) {

    var STORAGE_KEY = "addressBookStorage";
    var findIndex = require('lodash/array/findIndex');

    this.getContacts = function() {

      var storage = localStorageService.get(STORAGE_KEY);

      if (!storage) {
        localStorageService.set(STORAGE_KEY, []);
        storage = localStorageService.get(STORAGE_KEY)
      }
      return storage;
    }

    this.addContact = function(contact) {

      contact.id = guid();

      var contacts = this.getContacts();
      contacts.push(contact);

      localStorageService.set(STORAGE_KEY, contacts);
    }

    this.updateContact = function(contact) {

      var contacts = this.getContacts();
      var index = indexForId(contacts, contact.id);
      contacts[index] = contact;

      localStorageService.set(STORAGE_KEY, contacts);
    }

    this.deleteContact = function(contactId) {

      var contacts = this.getContacts();
      var index = indexForId(contacts, contactId);

      contacts.splice(index, 1);

      localStorageService.set(STORAGE_KEY, contacts);
    }

    function indexForId(contacts, id) {
      return findIndex(contacts, function(o) {
        return o.id == id;
      });
    }

    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }

  }

})();