(function() {
	'use strict';

	angular
		.module('addressBook')
		.controller('ContactListController', ['addressBookStorageService', ContactListController]);

	function ContactListController(addressBookStorageService) {

		this.items = addressBookStorageService.getContacts();
	}

})();