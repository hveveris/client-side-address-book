require("../app/shared/countries/countryListService.js");
require("../app/components/address-book/addressBookStorageService.js");

require("../app/components/contact-list/contactListController.js");
require("../app/components/contact-add/addContactController.js");
require("../app/components/contact-edit/editContactController.js");