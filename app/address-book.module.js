(function() {
	'use strict';

	require("../node_modules/angular/angular.min.js");
	require("../node_modules/angular-route/angular-route.min.js");
	require("../node_modules/angular-local-storage/dist/angular-local-storage.min.js");

	angular
		.module('addressBook', ['ngRoute', 'LocalStorageModule']);
})();