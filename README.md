#The JavaScript Challenge

## Client Side Address Book
Build a SPA (Single Page Application) address book using JavaScript, HTML and CSS.

### Functionality
1. You should be able to add, delete and edit contacts.
2. The data should be persisted (on the client), and loaded again when the application starts.
3. Add fitting validation to the different input fields.

### Form Fields
* First name (input, required)
* Last name (input, required)
* Email (input, required, valid email)
* Country (dropdown, required) - use the [country-list](https://www.npmjs.com/package/country-list) module to populate the list

### Note
* Even though this is a small project, structure and architecture should mimic a large project!
* Feel free to depend on any frameworks/libraries you think is suitable using NPM/Bower.
* We want the code you submit to be written by you, so don’t use skelletons/generators.
* Make sure the application work on Node 5.x, NPM 3.x and in latest Chrome and Firefox.

### Installation

```bash
$ npm install
$ gulp run
```

### Tools

* [Angular](https://angularjs.org)
* [Lodash](https://lodash.com)
* [Node](https://nodejs.org)
* [Gulp](http://gulpjs.com)
* [Browserify](http://browserify.org)
